
package Clase;

/**
 *
 * @author castr
 */
public class Kennya {
    private long carnet_estudiante;
    private String nombre_de_estudiante;
    private String apellidos_estudiante;

    public long getCarnet_estudiante() {
        return carnet_estudiante;
    }

    public void setCarnet_estudiante(long carnet_estudiante) {
        this.carnet_estudiante = carnet_estudiante;
    }

    public String getNombre_de_estudiante() {
        return nombre_de_estudiante;
    }

    public void setNombre_de_estudiante(String nombre_de_estudiante) {
        this.nombre_de_estudiante = nombre_de_estudiante;
    }

    public String getApellidos_estudiante() {
        return apellidos_estudiante;
    }

    public void setApellidos_estudiante(String apellidos_estudiante) {
        this.apellidos_estudiante = apellidos_estudiante;
    }
    
    

}


